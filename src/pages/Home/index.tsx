import React from 'react';
import { IProp, IState } from './type';
import './style.css';

import ToDoDisplay from './components/TodoDisplay';

class Home extends React.Component<IProp, IState> {
    constructor(props: IProp){
        super(props); 
        this.state = {
            temp_input: "",
            temp_message: "",
            input_list : [],
            showDataList: false
        
        };
    }
//React.ChangeEvent<HTMLInputElement
    inputHandler = (event: any) => {
        const { value } = event.target;
        this.setState({ temp_input: value });
    }

    messageHandler = (event: any) => {
        const { value } = event.target;
        this.setState({ temp_message: value });
    }

    btnAddHandler = () => {
        const { temp_input, temp_message, input_list } = this.state;
        const newInputList = [...input_list, { input: temp_input, message: temp_message},];
        
        this.setState({ input_list: newInputList, temp_input: "", temp_message: "" });
    }

    toggleShowDataListHandler = () =>{
        this.setState((prevState) => ({
            showDataList: !prevState.showDataList
        }));
    }

    render(){
        const {input_list, showDataList} = this.state
        console.log(this.state);
        return (
            <div>
                <h1 className='header'>My Home Page</h1>
                <div className='basic-form-container'>
                    
                    <input name="Title" type="text" value={this.state.temp_input} onChange={this.inputHandler} />
                    <textarea name="message" value={this.state.temp_message} onChange={this.messageHandler}></textarea>
                    <button className="btn" onClick={this.btnAddHandler}>Add</button>
            </div>
            <input type="checkbox" onChange={this.toggleShowDataListHandler } checked={showDataList} /> <span>Show Table</span>
                {showDataList && <ToDoDisplay input_list={input_list} />}
        </div>
    );
}
}
export default Home;