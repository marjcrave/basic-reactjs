import "../style.css";
import { ItoDoDisplayProp } from "../type";

function ToDoDisplay (props: ItoDoDisplayProp) {
  const { input_list } = props;
  console.log("input_list:", input_list)
  return (
    <div className="center-table">
    {input_list.length > 0 ? (
    <div className="table container">
      <table border={1} className="centered">
        <thead>
          <tr>
            <th>Title</th>
            <th>Message</th>
          </tr>
          </thead>
          <tbody>      
      {input_list.map((items, index) => (
        <tr key={index}>
          <td>{items.input}</td>
          <td>{items.message}</td>
          </tr>
        ))}
       </tbody>
       </table>
      </div>
    ) : (
    <h5>No list found</h5>
    )}
    </div>
    
  );
}
export default ToDoDisplay;
