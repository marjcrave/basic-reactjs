export interface IProp {}

export interface IState {
  temp_input : string;
  temp_message: string;
  input_list: ItoDo [];
  showDataList: boolean;
 
}

export interface ItoDo {
  input : string;
  message: string;

}

export interface ItoDoDisplayProp {
  input_list: ItoDo [];
}

